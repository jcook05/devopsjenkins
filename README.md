# README #

This README would normally document whatever steps are necessary to get your application up and running.

### What is this repository for? ###

This repo holds jenkins ansible playbooks


### Playbooks



**jenkins-ami**

  This playbook will build a Jenkins AMI fully equipped to build projects in common languages from the Amazon Linux AMI.  
  Driver script:  buildami - This is a packer script using  the builddevami.json as the packer file.   
  The packer ansible provisioner is being leveraged.   
   
  usage:  ./buildami builddevami.json



**jenkins-dotnet**

  This playbook will update the target jenkins to a specified dotnet version.
  
  usage:  ansible-playbook jenkins-dotnet.yml --user ec2-user --become --verbose

**jenkins-docker**

  This playbook installs docker-ce binaries and runs the docker daemon process (dockerd)
  
  usage:  ansible-playbook jenkins-docker.yml --user ec2-user --become --verbose

